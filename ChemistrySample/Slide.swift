//
//  Slide.swift
//  ChemistrySample
//
//  Created by Jmango on 10/11/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

class Slide {
    var id: Int
    var name: String
    var hasNote: Bool
    
    init(id: Int, name: String, hasNote: Bool){
        self.id = id
        self.name = name
        self.hasNote = hasNote
    }
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.hasNote = json["hasNote"].boolValue
    }
}