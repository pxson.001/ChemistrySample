//
//  UnitTableViewCell.swift
//  ChemistrySample
//
//  Created by JMango on 7/19/17.
//  Copyright © 2017 Matth Kelly. All rights reserved.
//

import Foundation


class UnitTableViewCell: UITableViewCell {
  
  @IBOutlet var unitLabel: UILabel!
  @IBOutlet var premiumLabel: UILabel!
  @IBOutlet var buyOrDownloadButton: UIButton!
  
  var unit: Unit? {
    didSet {
      if let data = unit {
        unitLabel.text = "Unit \(data.id): " + data.name
        premiumLabel.text = data.isPremium ? "Premium" : "Free"
        premiumLabel.textColor = data.isPremium ? UIColor.orangeColor() : UIColor.blueColor()
        buyOrDownloadButton.hidden = LectureUtil.isUnitDownloaded(unit!)
      }
    }
  }
  
}
