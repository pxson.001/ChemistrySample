//
//  ViewController.swift
//  ChemistrySample
//
//  Created by Jmango on 10/6/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import UIKit
import KASlideShow
import NVActivityIndicatorView
import Toast_Swift

var currentSlideIndex: UInt = 0
var currentSlides = [Slide]()
var totalSlide: Int = 0

class ViewController: UIViewController, KASlideShowDelegate, KASlideShowDataSource {
     
    let lessonPath = LocalData.downloadPath! + "/data_v2/\(selectedUnit!.id)/lessons/\(selectedLesson!.id)"
 
    @IBOutlet var slideShow: KASlideShow!
    
    var activityIndicatorView: NVActivityIndicatorView!
    
    var datasource: [NSObject] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentSlides = [Slide]()
        currentSlideIndex = 0
  
        activityIndicatorView = NVActivityIndicatorView(frame: CGRectZero)
        activityIndicatorView.frame = CGRectMake(0, 0, 30, 30)
        activityIndicatorView.center = view.center
        activityIndicatorView.color = UIColor.redColor()
        activityIndicatorView.type = .BallRotateChase
        activityIndicatorView.startAnimating()
        self.view.addSubview(activityIndicatorView)
        
        slideShow.datasource = self
        slideShow.delegate = self
        slideShow.delay = 3
        slideShow.transitionDuration = 1
        slideShow.transitionType = KASlideShowTransitionType.SlideVertical
        slideShow.imagesContentMode = UIViewContentMode.ScaleAspectFit
        slideShow.addGesture(.SwipeUp)
        
        self.loadLessonInfo { (lesson) in
            let slides = lesson.slides
            currentSlides = slides
            totalSlide = slides.count
            
            NSNotificationCenter.defaultCenter().postNotificationName("onPageChange", object: nil)
            for slide in slides {
                self.datasource.append(UIImage(contentsOfFile: self.lessonPath + "/" + String(slide.id))!)
            }
            self.slideShow.reloadData()
            self.activityIndicatorView.stopAnimating()
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onPageSelect), name:"onPageSelect", object: nil)
    }
    
    func loadLessonInfo(completeHandler: (Lesson) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var lesson = Lesson()
            let contentPath = self.lessonPath + "/content.json"
            let jsonData = NSData(contentsOfFile: contentPath)
            let json = JSON(data: jsonData!)
            lesson = Lesson(json: json)
                
            dispatch_async(dispatch_get_main_queue(), {
                    
                completeHandler(lesson)
                    
            })
        }
    }
    
    func tranparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true;
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideShow(slideShow: KASlideShow!, objectAtIndex index: UInt) -> NSObject! {
        return datasource[Int(index)]
    }

    func slideShowImagesNumber(slideShow: KASlideShow!) -> UInt {
        return UInt(datasource.count)
    }
    
    func slideShowWillShowPrevious(slideShow: KASlideShow!) {
  
    }
    
    func slideShowWillShowNext(slideShow: KASlideShow!) {
   
    }
    
    func slideShowDidShowNext(slideShow: KASlideShow!) {
        currentSlideIndex = slideShow.currentIndex
        
        if currentSlideIndex == UInt(datasource.count - 1){
            slideShow.removeGestureSwipeUp()
        } else {
            slideShow.addGesture(.Swipe)
        }
        NSNotificationCenter.defaultCenter().postNotificationName("onPageChange", object: nil)
    }
    
    func slideShowDidShowPrevious(slideShow: KASlideShow!) {
        currentSlideIndex = slideShow.currentIndex
        
        if currentSlideIndex == UInt(0) {
            slideShow.removeGestureSwipeDown()
        } else {
            slideShow.addGesture(.Swipe)
        }
        NSNotificationCenter.defaultCenter().postNotificationName("onPageChange", object: nil)
    }
    
    func slideShowDidShowMove(slideShow: KASlideShow!) {
        currentSlideIndex = slideShow.currentIndex
        
        if currentSlideIndex == UInt(0) {
            slideShow.addGesture(.Swipe)
            slideShow.removeGestureSwipeDown()
        } else if currentSlideIndex == UInt(datasource.count - 1){
            slideShow.addGesture(.Swipe)
            slideShow.removeGestureSwipeUp()
        } else {
            slideShow.addGesture(.Swipe)
        }
        NSNotificationCenter.defaultCenter().postNotificationName("onPageChange", object: nil)
    }
    
    func onPageSelect() {
        slideShow.move(currentSlideIndex)
        NSNotificationCenter.defaultCenter().postNotificationName("onPageChange", object: nil)
    }
    
}

