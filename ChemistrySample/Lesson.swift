//
//  Lesson.swift
//  ChemistrySample
//
//  Created by Jmango on 10/11/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

class Lesson {
    
    var id: Int
    var name: String
    var isPremium: Bool
    var slides: [Slide]
    var isDownloaded: Bool
    
    init() {
        self.id = 0
        self.name = "Unknown"
        self.isPremium = false
        self.isDownloaded = false
        self.slides = [Slide]()
    }
    
    init(id: Int, name: String, isPremium: Bool) {
        self.id = id
        self.name = name
        self.isPremium = isPremium
        self.isDownloaded = false
        self.slides = [Slide]()
    }
    
    init(id: Int, name: String, isPremium: Bool, slides: [Slide]) {
        self.id = id
        self.name = name
        self.isPremium = isPremium
        self.isDownloaded = false
        self.slides = slides
    }
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.isPremium = json["isPremium"].boolValue
        self.isDownloaded = false
        self.slides = [Slide]()
        
        let JSONArray = json["slides"].arrayValue
        for JSONObject in JSONArray {
            let slide = Slide(json: JSONObject)
            self.slides.append(slide)
        }
    }
    
}
