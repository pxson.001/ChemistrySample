//
//  SystemUtil.swift
//  ChemistrySample
//
//  Created by Elight on 10/20/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

class SystemUtil {
    
    static func showAlert(controller: UIViewController, title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        controller.presentViewController(alertController, animated: true, completion: nil)
    }
}