//
//  Unit.swift
//  ChemistrySample
//
//  Created by JMango on 7/18/17.
//  Copyright © 2017 Matth Kelly. All rights reserved.
//

import Foundation


class Unit {
  
  var id: Int
  var name: String
  var isPremium: Bool
  var lessons: [Lesson]
  
  init() {
    self.id = 0
    self.name = ""
    self.isPremium = false
    self.lessons = [Lesson]()
  }
  
  init(id: Int, name: String, lessons: [Lesson]){
    self.id = id
    self.name = name
    self.isPremium = false
    self.lessons = lessons
  }
  
  init(json: JSON){
    self.id = json["id"].intValue
    self.name = json["name"].stringValue
    self.isPremium = json["isPremium"].boolValue
    self.lessons = [Lesson]()
    
    let JSONArray = json["lessons"].arrayValue
    for JSONObject in JSONArray {
      let lesson = Lesson(json: JSONObject)
      lessons.append(lesson)
    }
  }

  
}
