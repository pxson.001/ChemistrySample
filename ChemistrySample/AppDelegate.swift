//
//  AppDelegate.swift
//  ChemistrySample
//
//  Created by Jmango on 10/6/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import UIKit
import SSZipArchive
import SwiftyStoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        let zipPath = NSBundle.mainBundle().pathForResource("data_v2", ofType: "zip")
        var isDir: ObjCBool = true
        
        if NSFileManager.defaultManager().fileExistsAtPath(LocalData.dataPath, isDirectory: &isDir) {
            print("Data is existed")
        } else {
            print("Data is not existed")
            SSZipArchive.unzipFileAtPath(zipPath!, toDestination: LocalData.downloadPath!)
        }

        /*
        SwiftyStoreKit.completeTransactions() { products in
            
            for product in products {
                
                if product.transactionState == .Purchased || product.transactionState == .Restored {
                    
                    print("purchased: \(product)")
                }
            }
        }
        
        SwiftyStoreKit.retrieveProductsInfo([ProductIds.prefix + "11"]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.priceLocale
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Could not retrieve product info: " + "Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(result.error)")
            }
        }
        */
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

