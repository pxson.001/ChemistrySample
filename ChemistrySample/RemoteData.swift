//
//  RemoteData.swift
//  ChemistrySample
//
//  Created by Elight on 10/20/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

class RemoteData {
    static let BASE_DOWNLOAD_URL = "https://api.backendless.com/2F1846AD-FD0B-5A72-FFF3-AEB12DCE0800/v1/files/chemistry-ios/"
  
    static let BASE_V2_DOWNLOAD_URL = "https://api.backendless.com/2F1846AD-FD0B-5A72-FFF3-AEB12DCE0800/v1/files/chemistry-ios-v2/"

}
