//
//  AppInfo.swift
//  ChemistrySample
//
//  Created by Jmango on 10/11/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

class AppInfo {
    
    var name: String
    var versionCode: Int
    var units: [Unit]
    
    init() {
        name = ""
        self.versionCode = 1
        self.units = [Unit]()
    }
    
    init(name: String, versionCode: Int, units: [Unit]){
        self.name = name
        self.versionCode = versionCode
        self.units = units
    }
    
    init(json: JSON){
        self.name = json["name"].stringValue
        self.versionCode = json["versionCode"].intValue
        self.units = [Unit]()
        
        let JSONArray = json["units"].arrayValue
        for JSONObject in JSONArray {
            let unit = Unit(json: JSONObject)
            units.append(unit)
        }
    }
    
}
