//
//  DrawerViewController.swift
//  ChemistrySample
//
//  Created by Jmango on 10/6/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DrawerViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var noteWebView: UIWebView!
    
    var activityIndicatorView: NVActivityIndicatorView!
    
    var currentSlide: Slide!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView = NVActivityIndicatorView(frame: CGRectZero)
        activityIndicatorView.frame = CGRectMake(0, 0, 30, 30)
        activityIndicatorView.center = view.center
        activityIndicatorView.color = UIColor.redColor()
        activityIndicatorView.type = .BallRotateChase
        activityIndicatorView.startAnimating()
        self.view.addSubview(activityIndicatorView)
        
        noteWebView.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        currentSlide = currentSlides[Int(currentSlideIndex)]
        if currentSlide.hasNote {
            loadAdditionalNote()
        } else {
            loadEmptyNote()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func loadAdditionalNote() {
        var path = LocalData.downloadPath! + "/data_v2/\(selectedUnit!.id)/lessons/\(selectedLesson!.id)/\(currentSlideIndex + 1).html"
        print("loadAdditionalNote \(currentSlideIndex)")
        if NSFileManager.defaultManager().fileExistsAtPath(path) {
            print("\(currentSlideIndex) exist")
        } else {
            path = NSBundle.mainBundle().resourcePath! + "/data_v2/empty.html"
            print("\(currentSlideIndex) doesn't exist")
        }
        
        let url = NSURL(fileURLWithPath: path)
        let request = NSURLRequest(URL: url)
        self.noteWebView.loadRequest(request)
    }
    
    func loadEmptyNote() {
        let path = LocalData.downloadPath! + "/data_v2/empty.html"
        
        let url = NSURL(fileURLWithPath: path)
        let request = NSURLRequest(URL: url)
        self.noteWebView.loadRequest(request)
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        print("Start load webview")
        activityIndicatorView.startAnimating()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("Finish load webview")
        activityIndicatorView.stopAnimating()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        print("Fail to load webview")
        activityIndicatorView.stopAnimating()
    }
    
}
