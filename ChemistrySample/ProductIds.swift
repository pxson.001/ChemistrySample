//
//  ProductIds.swift
//  ChemistrySample
//
//  Created by Elight on 12/22/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

struct ProductIds {
    static let prefix = "com.matthew.chemistryapp.lecture"
    static let unitPrefix = "com.matthew.chemistryapp.unit"
}
