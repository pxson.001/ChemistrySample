//
//  LectureUtil.swift
//  ChemistrySample
//
//  Created by Elight on 10/19/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation
import SSZipArchive

class LectureUtil {
  
  static func isUnitDownloaded(unit: Unit) -> Bool {
    let fileName = String(unit.id)
    let lessonPath = LocalData.downloadPath! + "/data_v2/\(fileName)"
    
    var isDir: ObjCBool = true
    
    if NSFileManager.defaultManager().fileExistsAtPath(lessonPath, isDirectory: &isDir) {
      return true
    }
    
    return false
  }
  
  static func isLessonDownloaded(unitId: Int, lesson: Lesson) -> Bool {
    let fileName = String(lesson.id)
    let lessonPath = LocalData.downloadPath! + "/data_v2/\(unitId)/lessons/\(fileName)"
    
    var isDir: ObjCBool = true
    
    if NSFileManager.defaultManager().fileExistsAtPath(lessonPath, isDirectory: &isDir) {
      return true
    }
    
    return false
  }
  
    static func isLessonDownloaded(lesson: Lesson) -> Bool {
        let fileName = String(lesson.id)
        let lessonPath = LocalData.downloadPath! + "/data_v2/lessons/\(fileName)"
        
        var isDir: ObjCBool = true
        
        if NSFileManager.defaultManager().fileExistsAtPath(lessonPath, isDirectory: &isDir) {
            return true
        }
        
        return false
    }
    
    static func isZipLessonDownloaded(lesson: Lesson) -> Bool {
        let fileName = String(lesson.id) + ".zip"
        let lessonPath = LocalData.downloadPath! + "/data_v2/lessons/\(fileName)"
        
        var isDir: ObjCBool = true
        
        if NSFileManager.defaultManager().fileExistsAtPath(lessonPath, isDirectory: &isDir) {
            return true
        }
        
        return false
    }
    
    static func unzipLessonDownloaded(lesson: Lesson) {
        let lessonPath = LocalData.downloadPath! + "/data_v2/lessons/"
        let zipFile = lessonPath + "/" + String(lesson.id) + ".zip"
        
        SSZipArchive.unzipFileAtPath(zipFile, toDestination: lessonPath)
        try! NSFileManager.defaultManager().removeItemAtPath(zipFile)
    }
  
  static func isZipUnitDownloaded(unit: Unit) -> Bool {
    let fileName = String(unit.id) + ".zip"
    let unitPath = LocalData.downloadPath! + "/data_v2/\(fileName)"
    
    var isDir: ObjCBool = true
    
    if NSFileManager.defaultManager().fileExistsAtPath(unitPath, isDirectory: &isDir) {
      return true
    }
    
    return false
  }
  
  static func unzipUnitDownloaded(unit: Unit) {
    let unitPath = LocalData.downloadPath! + "/data_v2/"
    let zipFile = unitPath + "/" + String(unit.id) + ".zip"
    
    SSZipArchive.unzipFileAtPath(zipFile, toDestination: unitPath)
    try! NSFileManager.defaultManager().removeItemAtPath(zipFile)
  }
  
}
