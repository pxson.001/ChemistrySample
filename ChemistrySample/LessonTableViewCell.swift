//
//  LessonTableViewCell.swift
//  ChemistrySample
//
//  Created by Jmango on 10/11/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import UIKit

class LessonTableViewCell: UITableViewCell {

    @IBOutlet var lessonLabel: UILabel!
    
    var lesson: Lesson? {
        didSet {
            if let data = lesson {
              lessonLabel.text = data.name
            }
        }
    }
    
}
