//
//  MultipleChoiceViewController.swift
//  ChemistrySample
//
//  Created by JMango on 8/22/17.
//  Copyright © 2017 Matth Kelly. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

var selectedQuiz: Lesson!


class MultipleChoiceViewController: UIViewController, UIWebViewDelegate {
  
  @IBOutlet var quizWebView: UIWebView!
  
  var activityIndicatorView: NVActivityIndicatorView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    activityIndicatorView = NVActivityIndicatorView(frame: CGRectZero)
    activityIndicatorView.frame = CGRectMake(0, 0, 30, 30)
    activityIndicatorView.center = view.center
    activityIndicatorView.color = UIColor.redColor()
    activityIndicatorView.type = .BallRotateChase
    activityIndicatorView.startAnimating()
    self.view.addSubview(activityIndicatorView)
    
    let arrowImage = UIImage(named: "ic_back")
    let backButtonFrame = CGRectMake(0, 5, 30, 25)
    let backButton = UIButton(frame: backButtonFrame)
    backButton.setImage(arrowImage, forState: UIControlState.Normal)
    backButton.addTarget(self, action: #selector(onBackPressed), forControlEvents: UIControlEvents.TouchUpInside)
    
    let leftTitleView = UIBarButtonItem(customView: backButton)
    let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
    spacer.width = -10;
    self.navigationItem.leftBarButtonItems = [spacer, leftTitleView]

    
    quizWebView.delegate = self
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    loadQuiz()
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
  }
  
  func loadQuiz() {
    var path = LocalData.downloadPath! + "/data_v2/\(selectedUnit!.id)/lessons/\(selectedQuiz!.id).html"
    
    if NSFileManager.defaultManager().fileExistsAtPath(path) {
      print("Quiz \(selectedQuiz!.id) exist")
    } else {
      path = NSBundle.mainBundle().resourcePath! + "/data/empty.html"
      print("Quiz \(selectedQuiz!.id) doesn't exist")
    }
    
    let url = NSURL(fileURLWithPath: path)
    let request = NSURLRequest(URL: url)
    self.quizWebView.loadRequest(request)
  }
  
  func loadEmptyNote() {
    let path = LocalData.downloadPath! + "/data_v2/empty.html"
    
    let url = NSURL(fileURLWithPath: path)
    let request = NSURLRequest(URL: url)
    self.quizWebView.loadRequest(request)
  }
  
  func webViewDidStartLoad(webView: UIWebView) {
    print("Start load webview")
    activityIndicatorView.startAnimating()
  }
  
  func webViewDidFinishLoad(webView: UIWebView) {
    print("Finish load webview")
    activityIndicatorView.stopAnimating()
  }
  
  func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
    print("Fail to load webview")
    activityIndicatorView.stopAnimating()
  }
  
  func onBackPressed() {
    self.navigationController?.popViewControllerAnimated(true)
  }
}
