//
//  LocalData.swift
//  ChemistrySample
//
//  Created by Elight on 10/20/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import Foundation

class LocalData {
    static let downloadPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first
    static let dataPath = downloadPath! + "/data_v2"
}
