//
//  MainNewViewController.swift
//  ChemistrySample
//
//  Created by JMango on 7/18/17.
//  Copyright © 2017 Matth Kelly. All rights reserved.
//


import UIKit
import NVActivityIndicatorView
import MBProgressHUD
import Alamofire
import SwiftyStoreKit

var selectedUnit: Unit?

class MainNewViewController: UIViewController {

  @IBOutlet var unitTableView: UITableView!
  
  var units = [Unit]()
  
  var activityIndicatorView: NVActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Initialize IndicatorView
    activityIndicatorView = NVActivityIndicatorView(frame: CGRectZero)
    activityIndicatorView.frame = CGRectMake(0, 0, 30, 30)
    activityIndicatorView.center = view.center
    activityIndicatorView.color = UIColor.redColor()
    activityIndicatorView.type = .BallRotateChase
    activityIndicatorView.startAnimating()
    self.view.addSubview(activityIndicatorView)
    
    
    self.unitTableView.dataSource = self
    self.unitTableView.delegate = self
    
    loadAppInfo { (appInfo) in
      self.units = appInfo.units
      self.unitTableView.reloadData()
      self.activityIndicatorView.stopAnimating()
    }
  }
  
  func loadAppInfo(completeHandler: (AppInfo) -> Void) {
    var appInfo = AppInfo()
    
    let path = LocalData.downloadPath! + "/data_v2/units.json"
    let jsonData = NSData(contentsOfFile: path)
    let json = JSON(data: jsonData!)
    appInfo = AppInfo(json: json)
    
    completeHandler(appInfo)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
  }
  
  func buyOrDownload(sender: UIButton) {
      let unit = units[sender.tag]
      let fileName = String(unit.id) + ".zip"
      let downloadUrl = RemoteData.BASE_V2_DOWNLOAD_URL + "/" + fileName
    
      // Initialize ProgressHUD
      let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
      progressHUD.showAnimated(true)
      progressHUD.mode = .AnnularDeterminate
      progressHUD.label.text = "Downloading"
      progressHUD.progress = Float(0)
    
      downloadUnit(unit, url: downloadUrl, completionHandler: { (progress, error) in
       if progress == nil {
         if error == nil {
           progressHUD.hideAnimated(true)
           self.unitTableView.reloadData()
           return
          } else {
            SystemUtil.showAlert(self, title: "Error", message: "Oops! Something's wrong. Please try again!")
          }
        } else {
            progressHUD.progress = Float(progress!)
        }
      })
    }
  
  
  
  func downloadUnit(unit: Unit, url: String, completionHandler: (Double?, NSError?) -> Void){
    let fileName = String(unit.id) + ".zip"
    let unitPath = LocalData.downloadPath! + "/data_v2/\(fileName)"
    
    var isDir : ObjCBool = false
    if NSFileManager.defaultManager().fileExistsAtPath(unitPath, isDirectory: &isDir){
      try! NSFileManager.defaultManager().removeItemAtPath(unitPath)
    }
    
    Alamofire.download(.GET, url, destination: { (url, response) -> NSURL in
      let pathComponent = "/data_v2/\(fileName)"
      
      let fileManager = NSFileManager.defaultManager()
      let directoryURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
      let fileUrl = directoryURL.URLByAppendingPathComponent(pathComponent)
      
      return fileUrl!
    })
      .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
        dispatch_async(dispatch_get_main_queue()) {
          let progress = Double(totalBytesRead) / Double(totalBytesExpectedToRead)
          completionHandler(progress, nil)
        }
      }
      .response { (request, response, data, error) in
        if error != nil {
          completionHandler(nil, error)
        }
        
        if LectureUtil.isZipUnitDownloaded(unit) {
          LectureUtil.unzipUnitDownloaded(unit)
          self.unitTableView.reloadData()
          completionHandler(nil, nil)
        }
    }
  }
  

    func makePurchase(productId: String) {
      var isPurchased = false
      let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
      progressHUD.mode = .Indeterminate
      progressHUD.label.text = "Your request is being processed."
      progressHUD.showAnimated(true)
    
      SwiftyStoreKit.purchaseProduct(productId) { result in
        progressHUD.hideAnimated(true)
        switch result {
        case .Success(let productId):
          print("Purchase Success: \(productId)")
          isPurchased = true
        case .Error(let error):
          print("Purchase Failed: \(error)")
          isPurchased = false
        }
        NSUserDefaults.standardUserDefaults().setBool(isPurchased, forKey: productId)
        self.unitTableView.reloadData()
      }
    }

  @IBAction func restorePurchase(sender: AnyObject) {
    SwiftyStoreKit.restorePurchases { (results) in
      
      if results.restoreFailedProducts.count > 0 {
        print("Restore Failed: \(results.restoreFailedProducts)")
      } else if results.restoredProductIds.count > 0 {
        for productId in results.restoredProductIds {
          NSUserDefaults.standardUserDefaults().setBool(true, forKey: productId)
        }
        self.unitTableView.reloadData()
      } else {
        print("Nothing to Restore")
      }
      
    }

  }
}


extension MainNewViewController: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return units.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("UnitCell") as! UnitTableViewCell
    
    let unit = units[indexPath.row]
    let productId = ProductIds.unitPrefix + "\(unit.id)"
    let userAlreadyBought = NSUserDefaults.standardUserDefaults().boolForKey(productId)

    cell.unit = unit
    cell.buyOrDownloadButton.enabled = userAlreadyBought
    cell.buyOrDownloadButton.tag = indexPath.row
    cell.buyOrDownloadButton.addTarget(self, action: #selector(MainNewViewController.buyOrDownload(_:)), forControlEvents: .TouchUpInside)
    
    return cell
  }
}

extension MainNewViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
      let unit = units[indexPath.row]
      let productId = ProductIds.unitPrefix + "\(unit.id)"
      let userAlreadyBought = NSUserDefaults.standardUserDefaults().boolForKey(productId)
    
    
    
      if(unit.isPremium && !userAlreadyBought) {
        makePurchase(productId)
        return
      }
 
    
    
    if LectureUtil.isUnitDownloaded(unit) {
      selectedUnit = unit
      self.performSegueWithIdentifier("unitSegue", sender: self)
    } else {
      self.view.makeToast("You need to download this lecture", duration: 3, position: .Bottom)
    }
  }
}
