//
//  MainViewController.swift
//  ChemistrySample
//
//  Created by Jmango on 10/6/16.
//  Copyright © 2016 Matth Kelly. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import MBProgressHUD
import SwiftyStoreKit


var selectedLesson: Lesson?

class MainViewController: UIViewController {
  
  @IBOutlet var lessonTableView: UITableView!
  
  var lessons = [Lesson]()
  
  var quizes = [Lesson]()
  
  var activityIndicatorView: NVActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Initialize IndicatorView
    activityIndicatorView = NVActivityIndicatorView(frame: CGRectZero)
    activityIndicatorView.frame = CGRectMake(0, 0, 30, 30)
    activityIndicatorView.center = view.center
    activityIndicatorView.color = UIColor.redColor()
    activityIndicatorView.type = .BallRotateChase
    activityIndicatorView.startAnimating()
    self.view.addSubview(activityIndicatorView)
    
    
    self.lessonTableView.dataSource = self
    self.lessonTableView.delegate = self
    
    
    let arrowImage = UIImage(named: "ic_back")
    let backButtonFrame = CGRectMake(0, 5, 30, 25)
    let backButton = UIButton(frame: backButtonFrame)
    backButton.setImage(arrowImage, forState: UIControlState.Normal)
    backButton.addTarget(self, action: #selector(onBackPressed), forControlEvents: UIControlEvents.TouchUpInside)
    
    let leftTitleView = UIBarButtonItem(customView: backButton)
    let spacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
    spacer.width = -10;
    self.navigationItem.leftBarButtonItems = [spacer, leftTitleView]


    
    loadUnitInfo { (unit) in
      self.lessons = unit.lessons
      self.loadQuizes(self.lessons)
      self.lessonTableView.reloadData()
      self.activityIndicatorView.stopAnimating()
      
      self.title = "Unit \(selectedUnit!.id)"
    }
  }
  
  func loadUnitInfo(completeHandler: (Unit) -> Void) {
    var unit = Unit()
    
    let path = LocalData.downloadPath! + "/data_v2/\(selectedUnit!.id)/content.json"
    let jsonData = NSData(contentsOfFile: path)
    let json = JSON(data: jsonData!)
    unit = Unit(json: json)
    
    completeHandler(unit)
  }
  
  func loadQuizes(lessons: [Lesson]) {
    for lesson in lessons {
      if lesson.isPremium {
        quizes.append(lesson)
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
  }
  
  func onBackPressed() {
    self.navigationController?.popViewControllerAnimated(true)
  }
  
  //    @IBAction func buyOrDownload(sender: UIButton) {
  //
  //            let lesson = lessons[sender.tag]
  //            let fileName = String(lesson.id) + ".zip"
  //            let downloadUrl = RemoteData.BASE_DOWNLOAD_URL + "/" + fileName
  //
  //
  //            // Initialize ProgressHUD
  //            let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
  //            progressHUD.showAnimated(true)
  //            progressHUD.mode = .AnnularDeterminate
  //            progressHUD.label.text = "Downloading"
  //            progressHUD.progress = Float(0)
  //
  //            downloadLesson(lesson, url: downloadUrl, completionHandler: { (progress, error) in
  //                if progress == nil {
  //                    if error == nil {
  //                        progressHUD.hideAnimated(true)
  //                        lesson.isDownloaded = true
  //                        self.lessonTableView.reloadData()
  //                        return
  //                    } else {
  //                        SystemUtil.showAlert(self, title: "Error", message: "Oops! Something's wrong. Please try again!")
  //                    }
  //                } else {
  //                    progressHUD.progress = Float(progress!)
  //                }
  //            })
  //
  //
  //    }
  
  
  /*
  
      func downloadLesson(lesson: Lesson, url: String, completionHandler: (Double?, NSError?) -> Void){
          let fileName = String(lesson.id) + ".zip"
          let lessonPath = LocalData.downloadPath! + "/data_v2/lessons/\(fileName)"
  
          var isDir : ObjCBool = false
          if NSFileManager.defaultManager().fileExistsAtPath(lessonPath, isDirectory: &isDir){
              try! NSFileManager.defaultManager().removeItemAtPath(lessonPath)
          }
  
          Alamofire.download(.GET, url, destination: { (url, response) -> NSURL in
                let pathComponent = "/data_v2/lessons/\(fileName)"

                let fileManager = NSFileManager.defaultManager()
                  let directoryURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
                  let fileUrl = directoryURL.URLByAppendingPathComponent(pathComponent)
  
                  return fileUrl!
              })
              .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
                  dispatch_async(dispatch_get_main_queue()) {
                      let progress = Double(totalBytesRead) / Double(totalBytesExpectedToRead)
                      completionHandler(progress, nil)
                  }
              }
              .response { (request, response, data, error) in
                  if error != nil {
                      completionHandler(nil, error)
                  }
  
                  if LectureUtil.isZipLessonDownloaded(lesson) {
                      LectureUtil.unzipLessonDownloaded(lesson)
                      self.lessonTableView.reloadData()
                      completionHandler(nil, nil)
                }
            }
      }
  */
 
  
  //    func makePurchase(productId: String) {
  //        var isPurchased = false
  //        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
  //        progressHUD.mode = .Indeterminate
  //        progressHUD.label.text = "Your request is being processed."
  //        progressHUD.showAnimated(true)
  //
  //        SwiftyStoreKit.purchaseProduct(productId) { result in
  //            progressHUD.hideAnimated(true)
  //            switch result {
  //            case .Success(let productId):
  //                print("Purchase Success: \(productId)")
  //                isPurchased = true
  //            case .Error(let error):
  //                print("Purchase Failed: \(error)")
  //                isPurchased = false
  //            }
  //            NSUserDefaults.standardUserDefaults().setBool(isPurchased, forKey: productId)
  //            self.lessonTableView.reloadData()
  //        }
  //    }
  //
  //    @IBAction func restorePurchases(sender: AnyObject) {
  //        SwiftyStoreKit.restorePurchases { (results) in
  //
  //            if results.restoreFailedProducts.count > 0 {
  //                print("Restore Failed: \(results.restoreFailedProducts)")
  //            } else if results.restoredProductIds.count > 0 {
  //                for productId in results.restoredProductIds {
  //                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: productId)
  //                }
  //                self.lessonTableView.reloadData()
  //            } else {
  //                print("Nothing to Restore")
  //            }
  //
  //        }
  //    }
  
}


extension MainViewController: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return lessons.count - quizes.count
    }
    return quizes.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("LessonCell") as! LessonTableViewCell
    
    
    var lesson = lessons[indexPath.row]
    if indexPath.section == 1 {
      lesson = lessons[lessons.count - 2 + indexPath.row]
    }
    
    cell.lesson = lesson
    
    return cell
  }
  
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if section == 0 {
      return "Lessons"
    }
    return "Multiple choice questions"
  }
}

extension MainViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    var lesson = lessons[indexPath.row]
    if indexPath.section == 1 {
      lesson = lessons[lessons.count - 2 + indexPath.row]
    }
    
    if !lesson.isPremium {
      selectedLesson = lesson
      self.performSegueWithIdentifier("lessonSegue", sender: self)
    } else {
      selectedQuiz = lesson
      self.performSegueWithIdentifier("quizSegue", sender: self)
    }
  }
}
